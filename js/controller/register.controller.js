﻿(function () {
    'use strict';

    angular
        .module('app')
        .controller('RegisterController', RegisterController);
        
    function RegisterController(UserService, $location, $rootScope, MsgService) {
        var vm = this;

        vm.register = register;

        function register() {
            vm.dataLoading = true;
            UserService.Create(vm.user).then(function (response) {
                if (response.success) {
                    MsgService.Success('Registro Exitoso!!', true);
                    $location.path('/login');
                } else {
                    MsgService.Error(response.message);
                    vm.dataLoading = false;
                }
            });
        }
    }

})();
