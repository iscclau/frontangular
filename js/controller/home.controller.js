﻿(function () {
    'use strict';

    angular
        .module('app')
        .controller('HomeController', HomeController);

    function HomeController(UserService, $location, $rootScope, MsgService, ArticleService, ComentaryService ) {
        var vm = this;

        //Definición de las variables
        vm.user = null;
        vm.artic = null;
        vm.coment = null;
        vm.count = 0;
        vm.allUsers = [];
        vm.artsInf = [];
        vm.allArts = [];
        vm.allComents = [];
        vm.allComen = [];

        //Definición de las funciones que se utilizaran en el html
        vm.deleteUser = deleteUser;
        vm.updateUser = updateUser;
        vm.registerArt = registerArt;
        vm.loadCurrentArt = loadCurrentArt;
        vm.deleteArt = deleteArt;
        vm.updateArt = updateArt;
        vm.registerCom = registerCom;
        vm.loadComByIdArt = loadComByIdArt;
        vm.deleteCom = deleteCom;
        vm.loadArticleUser = loadArticleUser;

        //Inicialización de métodos 
        
        initController();

        function initController() {
            loadCurrentUser();
            loadAllUsers();
            loadAllArticle();
            loadAllComentary();
        }

        //Definición de las funciones para mandar a llamar los servicios para realizar las acciones pertinentes de acuerdo a lo que se reuiere
        //Definición de las funciones para el control de la información del usuario
        function loadCurrentUser() {
            UserService.GetByUsername($rootScope.globals.currentUser.username).then(function (user) {
                vm.user = user;
            });
        }

        function loadAllUsers() {
            UserService.GetAll().then(function (users) {
                vm.allUsers = users;
            });
        }

        function deleteUser(id) {
            UserService.Delete(id).then(function () {
                loadAllUsers();
            });
        }

        function updateUser(user){
            vm.dataLoading = true;
            UserService.Update(user).then(function () {
                vm.userInf = user;
            });
        }

        /***********************************************************************************************************************/
        //Definición de las funciones para el control de la información de los articulos

        function registerArt(){
            vm.articleLoading = true;
            ArticleService.CreateArticle(vm.art).then(function(response){
                if (response.success) {
                    MsgService.Success('Registro Exitoso!!', true);
                    $location.path('/');
                } else {
                    MsgService.Error(response.message);
                    vm.articleLoading = false;
                }
            });
        }

        function loadAllArticle(){
            ArticleService.GetAllArticle().then(function(arts){
                vm.allArts = arts;
            });
        }

       /* function loadArticleUser(idUser){
            vm.articleLoading = true;
            ArticleService.GetArticleByUser(idUser).then(function(arts){
                vm.artsInf = arts;
            });
        }*/

        function loadArticleUser(idUser){
            loadAllArticle();
            var articu = vm.allArts;

            for(var i = 0 ; i < articu.length; i++){
               if(articu[i].idUser === idUser){
                    var art1 = articu[i];
                    vm.artsInf.push(art1);
               } 
            }
            console.log(vm.artsInf);
        }

        function deleteArt(idArt) {
            ArticleService.DeleteArticle(idArt).then(function () {
                loadAllArticle();
            });
        }

        function loadCurrentArt(idArt) {
            ArticleService.GetArticleById(idArt).then(function(art) {
                vm.artic = art;
            });
        }

        function updateArt(artic){
            vm.articleUpLoading = true;
            ArticleService.UpdateArticle(artic).then(function(){
                vm.artInfo = artic;
            });
        }

        /***********************************************************************************************************************/
        //Definición de las funciones para el control de la información de los comentarios

        function registerCom(){
            vm.comenLoading = true;
            ComentaryService.CreateComentary(vm.com).then(function(response){
                if (response.success) {
                    MsgService.Success('Registro Exitoso!!', true);
                    $location.path('/');
                } else {
                    MsgService.Error(response.message);
                    vm.comenLoading = false;
                }
            });
        }

        function registerArt(){
            vm.articleLoading = true;
            ArticleService.CreateArticle(vm.art).then(function(response){
                if (response.success) {
                    MsgService.Success('Registro Exitoso!!', true);
                    $location.path('/');
                } else {
                    MsgService.Error(response.message);
                    vm.articleLoading = false;
                }
            });
        }

        function loadAllComentary(){
            ComentaryService.GetAllComentary().then(function(coments){
                vm.allComents = coments;
            });
        }

        function loadComByIdArt(idArtC){
            loadAllComentary();
            var comts = vm.allComents;

            for(var i = 0 ; i < comts.length; i++){
               if(comts[i].idArtC === idArtC){
                    var com1 = comts[i];
                    vm.allComen.push(com1);
               } 
            }
            console.log(vm.allComen);
        }

        function deleteCom(idCom) {
            ComentaryService.DeleteComentary(idCom).then(function () {
                loadAllComentary();
            });
        }

    }

})();
