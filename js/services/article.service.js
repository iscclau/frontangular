(function(){
	'use strict';

	angular
		.module('app')
		.factory('ArticleService', ArticleService);

	function ArticleService($timeout, $filter, $q){
		var service = {};

        //Declaración de servicios
        
		service.GetAllArticle = GetAllArticle;
		service.GetArticleById = GetArticleById;
		service.GetByArticle = GetByArticle;
		service.CreateArticle = CreateArticle;
		service.UpdateArticle = UpdateArticle;
		service.DeleteArticle = DeleteArticle;
        service.GetArticleByUser = GetArticleByUser;

		return service;

		//Definición de las funciones para ejecutar cada uno de los procesos

		 function GetAllArticle() {
            var deferred = $q.defer();
            deferred.resolve(getArticle());
            return deferred.promise;
        }

        function GetArticleById(idArt) {
            var deferred = $q.defer();
            var filtered = $filter('filter')(getArticle(), { idArt: idArt });
            var art = filtered.length ? filtered[0] : null;
            deferred.resolve(art);
            return deferred.promise;
        }

        function GetByArticle(nameArt) {
            var deferred = $q.defer();
            var filtered = $filter('filter')(getArticle(), { nameArt: nameArt });
            var art = filtered.length ? filtered[0] : null;
            deferred.resolve(art);
            return deferred.promise;
        }

        function CreateArticle(art) {
            var deferred = $q.defer();
           
            $timeout(function () {
                GetByArticle(art.nameArt)
                    .then(function (duplicateArt) {
                        if (duplicateArt !== null) {
                            deferred.resolve({ success: false, message: 'Artículo "' + art.nameArt + '" creado anteriormente!!' });
                        } else {
                            var arts = getArticle();

                            var lastArt = arts[arts.length - 1] || { idArt: 0 };
                            art.idArt = lastArt.idArt + 1;
    
                            arts.push(art);
                            setArticle(arts);

                            deferred.resolve({ success: true });
                        }
                    });
            }, 1000);

            return deferred.promise;
        }

        function UpdateArticle(art) {
            var deferred = $q.defer();

            var arts = getArticle();
            for (var i = 0; i < arts.length; i++) {
                if (arts[i].idArt === art.idArt) {
                    arts[i] = art;
                    break;
                }
            }
            setArticle(arts);
            deferred.resolve();

            return deferred.promise;
        }

        function DeleteArticle(idArt) {
            var deferred = $q.defer();

            var arts = getArticle();
            for (var i = 0; i < arts.length; i++) {
                var art = arts[i];
                if (art.idArt === idArt) {
                    arts.splice(i, 1);
                    break;
                }
            }
            setArticle(arts);
            deferred.resolve();

            return deferred.promise;
        }

        function GetArticleByUser(idUser) {
            var deferred = $q.defer();

            var arts = getArticle();
            for (var i = 0; i < arts.length; i++) {
                if (arts[i].idUser === art.idUser) {
                    arts[i] = art;
                    break;
                }
            }
            setArticle(arts);
            deferred.resolve();

            return deferred.promise;
        }
      

        function getArticle() {
            if(!localStorage.arts){
                localStorage.arts = JSON.stringify([]);
            }

            return JSON.parse(localStorage.arts);
        }

        function setArticle(arts) {
            localStorage.arts = JSON.stringify(arts);
        }
    }
	
})();