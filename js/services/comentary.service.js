(function(){
	'use strict';

	angular
		.module('app')
		.factory('ComentaryService', ComentaryService);

	function ComentaryService($timeout, $filter, $q){
		var service = {};

		//Declaración de Servicios
		
		service.GetAllComentary = GetAllComentary;
		service.GetComByIdArt = GetComByIdArt;
		service.GetByComentary = GetByComentary;
		service.CreateComentary = CreateComentary;
		service.DeleteComentary = DeleteComentary;

		return service;

		function GetAllComentary() {
            var deferred = $q.defer();
            deferred.resolve(getComentary());
            return deferred.promise;
        }

        function GetComByIdArt(idArtC) {
            var deferred = $q.defer();
            var filtered = $filter('filter')(getComentary(), { idArtC: idArtC });
            var com = filtered.length ? filtered[0] : null;
            deferred.resolve(com);
            return deferred.promise;
        }

        function GetByComentary(titlee) {
            var deferred = $q.defer();
            var filtered = $filter('filter')(getComentary(), { titlee: titlee });
            var com = filtered.length ? filtered[0] : null;
            deferred.resolve(com);
            return deferred.promise;
        }

        function CreateComentary(com) {
            var deferred = $q.defer();
           
            $timeout(function () {
                GetByComentary(com.titlee)
                    .then(function (duplicateComent) {
                        if (duplicateComent !== null) {
                            deferred.resolve({ success: false, message: 'Comentario "' + com.titlee + '" creado anteriormente!!' });
                        } else {
                            var coments = getComentary();

                            var lastCom = coments[coments.length - 1] || { idCom: 0 };
                            com.idCom = lastCom.idCom + 1;
    
                            coments.push(com);
                            setComentary(coments);

                            deferred.resolve({ success: true });
                        }
                    });
            }, 1000);

            return deferred.promise;
        }

        function DeleteComentary(idCom) {
            var deferred = $q.defer();

            var coments = getComentary();
            for (var i = 0; i < coments.length; i++) {
                var com = coments[i];
                if (com.idCom === idCom) {
                    coments.splice(i, 1);
                    break;
                }
            }
            setComentary(coments);
            deferred.resolve();

            return deferred.promise;
        }

        function getComentary() {
            if(!localStorage.coments){
                localStorage.coments = JSON.stringify([]);
            }

            return JSON.parse(localStorage.coments);
        }

        function setComentary(coments) {
            localStorage.coments = JSON.stringify(coments);
        }
	}
})();